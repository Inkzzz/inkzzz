package me.bukkit.cMang.Inkzzz.Commands;

import me.bukkit.cMang.Inkzzz.Chat;
import me.bukkit.cMang.Inkzzz.Utils.Color;
import me.bukkit.cMang.Inkzzz.Utils.Messages;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MuteChat implements CommandExecutor {

	public static boolean chat = false;

	public boolean onCommand(CommandSender sender, Command cmd, String string,
			String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(Color.color("&4You don't have permission!"));
			return true;
		}
		Player p = (Player) sender;

		if (cmd.getName().equalsIgnoreCase("mutechat")) {
			if (Chat.getInstance().hasPerm(p, "mutechat")) {
				if (chat) {
					Bukkit.broadcastMessage(Messages.getMuteChatOnMSG(p));
					chat = false;
					return true;
				} else {
					Bukkit.broadcastMessage(Messages.getMuteChatOFFMSG(p));
					chat = true;
					return true;
				}
			}
		}
		return false;
	}

}
