package me.bukkit.aRepair.Inkzzz;

import java.io.File;
import java.io.IOException;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;

public class DataManager {
	
	private DataManager() {}

	static DataManager instance = new DataManager();

	public static DataManager getInstance() {
		return instance;
	}

	Plugin p;

	FileConfiguration data;
	File datamanager;

	public void setUp(Plugin p) {

		datamanager = new File(p.getDataFolder(), "data.yml");

		if (!datamanager.exists()) {
			try {
				datamanager.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				Bukkit.getServer()
						.getLogger()
						.severe(ChatColor.RED
								+ "Couldn't create data.yml file!");
			}
		}
				
		data = YamlConfiguration.loadConfiguration(datamanager);

	}
	
	public FileConfiguration getData() {
		return data;
	}
	
	public void saveData() {
		try {
			data.save(datamanager);
		} catch (IOException e) {
			e.printStackTrace();
			Bukkit.getServer().getLogger().severe(ChatColor.RED + "Failed to save data.yml file!");
		}
	}
	
	public void reloadData() {
		
		data = YamlConfiguration.loadConfiguration(datamanager);
		
	}
	
	public PluginDescriptionFile getDesc() {
		return p.getDescription();
	}
}

