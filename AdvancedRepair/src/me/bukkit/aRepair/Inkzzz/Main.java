package me.bukkit.aRepair.Inkzzz;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	DataManager dm = DataManager.getInstance();
	
	public void onEnable() {
		getConfig().options().copyDefaults(true);
		saveDefaultConfig();
		dm.setUp(this);
		loadCommands();
	}
	
	public void onDisable() {
		
	}
	
	private void loadCommands() {
		getCommand("fix").setExecutor(new RepairCMD());
	}	
}
