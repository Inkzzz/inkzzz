package me.bukkit.cMang.Inkzzz.Listeners;

import me.bukkit.cMang.Inkzzz.Chat;
import me.bukkit.cMang.Inkzzz.Commands.MuteChat;
import me.bukkit.cMang.Inkzzz.Utils.Messages;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerChat implements Listener {

	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		final Player p = e.getPlayer();

		String msg = e.getMessage();

		if (MuteChat.chat) {
			if (!p.hasPermission("mutechat.bypass")) {
				e.setCancelled(true);
				p.sendMessage(Messages.getMutedChatMSG());
				return;
			}
		}

		for (String s : Chat.getInstance().getConfig()
				.getStringList("Chat.Blacklist")) {
			if (msg.toLowerCase().contains(s)) {
				if (!p.hasPermission("blacklist.bypass")) {
					e.setCancelled(true);
					p.sendMessage(Messages.getNoSwearMSG());
					return;
				}
			}
		}

		if (!p.hasPermission("Chat.cooldown")) {

			if (Chat.chatCD.containsKey(p.getName())) {
				e.setCancelled(true);
				p.sendMessage(Messages.getChatCooldownMSG(p));
				return;
			} else {

				Chat.chatCD.put(p.getName(), Chat.getInstance().getConfig()
						.getInt("Chat.Cooldown"));
				Chat.chatCDrun.put(p.getName(), new BukkitRunnable() {

					@Override
					public void run() {

						if (Chat.chatCD.containsKey(p.getName())) {
							Chat.chatCD.put(p.getName(),
									Chat.chatCD.get(p.getName()) - 1);
						}

						if (Chat.chatCD.get(p.getName()) <= 0) {
							Chat.chatCD.remove(p.getName());
							Chat.chatCDrun.remove(p.getName());
							cancel();
						}

					}

				});

				Chat.chatCDrun.get(p.getName()).runTaskTimer(
						Chat.getInstance(), 20, 20);
				return;
			}
		}
	}
}
