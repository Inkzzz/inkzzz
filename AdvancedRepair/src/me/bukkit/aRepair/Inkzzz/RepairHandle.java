package me.bukkit.aRepair.Inkzzz;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class RepairHandle {

	static DataManager dm = DataManager.getInstance();

	public static boolean inCooldown(Player p) {
		if (dm.getData().getString("Cooldown." + p.getUniqueId().toString()) != null) {
			return true;
		} else
			return false;
	}

	public static void setCooldown(Player p) {
		dm.getData().set("Cooldown." + p.getUniqueId().toString() + ".DayUsed",
				System.currentTimeMillis());
		dm.getData().set(
				"Cooldown." + p.getUniqueId().toString() + ".TimeLeft",
				System.currentTimeMillis() + 43200000);
		dm.saveData();
	}

	public static boolean canRepair(Player p) {
		if (System.currentTimeMillis() > getTimeLeft(p)) {
			return false;
		} else {
			dm.getData().set("Cooldown." + p.getUniqueId().toString(), null);
			dm.saveData();
			return true;
		}
	}

	public static int getTimeLeft(Player p) {
		return dm.getData().getInt(
				"Cooldown." + p.getUniqueId().toString() + ".TimeLeft");
	}

	public static int getDayUsed(Player p) {
		return dm.getData().getInt(
				"Cooldown." + p.getUniqueId().toString() + ".DayUsed");
	}

	public static void sendCooldownMSG(Player p) {

		int timeleft = (int) (getTimeLeft(p) - System.currentTimeMillis());
		int seconds = (int) ((timeleft / 1000) % 60);
		int minutes = (int) ((timeleft / 1000) / 60);
		int hours = (int) ((minutes / 60) % 24);

		if (hours > 1) {
			p.sendMessage(ChatColor.translateAlternateColorCodes('&',
					"&cRepair> &7You can repair again in &e" + hours
							+ " &7hour(s). "));
		} else if (hours < 1 && minutes > 1) {
			p.sendMessage(ChatColor.translateAlternateColorCodes(
					'&',
					"&cRepair> &7You can repair again in &e" + minutes
							+ " &7minutes(s).").replace("-", ""));
		} else if (minutes < 1 && seconds > 0) {
			p.sendMessage(ChatColor.translateAlternateColorCodes(
					'&',
					"&cRepair> &7You can repair again in &e" + seconds
							+ " &7seconds(s).").replace("-", ""));
		} else {
			dm.getData().set("Cooldown." + p.getUniqueId().toString(), null);
			dm.saveData();
			p.sendMessage(ChatColor
					.translateAlternateColorCodes(
							'&',
							"&cRepair> &7To confirm to repair all your inventory items, run the command &e/fix all&7."));
		}
	}

	public static boolean isRepairable(ItemStack item) {
		if (item.getType() == Material.POTION)
			return false;
		if (item.getType() == Material.GOLDEN_APPLE) {
			if (item.getDurability() == 1)
				return false;
		}
		return true;
	}

}
