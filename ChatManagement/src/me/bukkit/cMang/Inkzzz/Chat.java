package me.bukkit.cMang.Inkzzz;

import java.util.HashMap;

import me.bukkit.cMang.Inkzzz.Commands.ClearChat;
import me.bukkit.cMang.Inkzzz.Commands.MainCMD;
import me.bukkit.cMang.Inkzzz.Commands.MuteChat;
import me.bukkit.cMang.Inkzzz.Listeners.PlayerChat;
import me.bukkit.cMang.Inkzzz.Utils.Messages;

import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Chat extends JavaPlugin {
	
	public static HashMap<String, Integer> chatCD = new HashMap<>();
	public static HashMap<String, BukkitRunnable> chatCDrun = new HashMap<>();
		
	static Chat instance;

	public static Chat getInstance() {
		return instance;
	}

	public void onEnable() {
		instance = this;
		getConfig().options().copyDefaults(true);
		saveDefaultConfig();
		loadCommands();
		loadListeners();
		resetBooleans();
	}

	public void onDisable() {
		instance = null;
	}

	private void loadCommands() {
		getCommand("cc").setExecutor(new ClearChat());
		getCommand("mutechat").setExecutor(new MuteChat());
		getCommand("chatmanagement").setExecutor(new MainCMD());
	}
	
	private void loadListeners() {
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new PlayerChat(), this);
	}
	
	private void resetBooleans() {
		MuteChat.chat = false;
	}
	
	public boolean hasPerm(Player p, String name) {

		if (p.hasPermission("ChatManagement." + name)) {
			return true;
		} else {
			p.sendMessage(Messages.noPerm());
			return false;
		}
	}

}
