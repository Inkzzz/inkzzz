package me.bukkit.cMang.Inkzzz.Utils;

import me.bukkit.cMang.Inkzzz.Chat;

import org.bukkit.entity.Player;

public class Messages {

	static Chat plugin = Chat.getInstance();

	public static String noPerm() {
		return Color.color(plugin.getConfig()
				.getString("Messages.NoPermission"));
	}

	public static String getClearChatMSG(Player p) {
		return Color.color(plugin.getConfig().getString("Messages.ClearChat")
				.replaceAll("%player%", p.getName()));
	}

	public static String getMuteChatOnMSG(Player p) {
		return Color.color(plugin.getConfig().getString("Messages.MuteChatON")
				.replaceAll("%player%", p.getName()));
	}

	public static String getMuteChatOFFMSG(Player p) {
		return Color.color(plugin.getConfig().getString("Messages.MuteChatOFF")
				.replaceAll("%player%", p.getName()));
	}

	public static String getMutedChatMSG() {
		return Color.color(plugin.getConfig().getString("Messages.ChatMuted"));
	}

	public static String getChatCooldownMSG(Player p) {
		return Color.color(plugin.getConfig()
				.getString("Messages.ChatCooldown"));
	}

	public static String getNoSwearMSG() {
		return Color.color(plugin.getConfig().getString("Messages.NoSwear"));
	}

	public static String getAddedWordMSG(String word) {
		return Color.color(plugin.getConfig()
				.getString("Messages.AddedBlacklistWord")
				.replaceAll("%word%", word));
	}

	public static String getRemovedWordMSG(String word) {
		return Color.color(plugin.getConfig()
				.getString("Messages.RemovedBlacklistWord")
				.replaceAll("%word%", word));
	}

	public static String getWordNotInBlacklistMSG(String word) {
		return Color.color(plugin.getConfig()
				.getString("Messages.WordNotInBlacklist")
				.replaceAll("%word%", word));
	}

}
