package me.bukkit.cMang.Inkzzz.Commands;

import java.util.ArrayList;
import java.util.List;

import me.bukkit.cMang.Inkzzz.Chat;
import me.bukkit.cMang.Inkzzz.Utils.Color;
import me.bukkit.cMang.Inkzzz.Utils.Messages;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MainCMD implements CommandExecutor {

	List<String> list = Chat.getInstance().getConfig()
			.getStringList("Chat.Blacklist");
	ArrayList<String> wordList = new ArrayList<String>();

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string,
			String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (cmd.getName().equalsIgnoreCase("chatmanagement")) {
				if (Chat.getInstance().hasPerm(p, "admin")) {
					if (args.length == 1) {
						if (args[0].equalsIgnoreCase("list")) {
							wordList.addAll(list);
							p.sendMessage(Color.color("&aWord Blacklist &8&l"
									+ Color.arrow()
									+ " &7"
									+ wordList.toString().replace("[", "")
											.replace("]", "")));
							wordList.clear();
						}
					} else if (args.length == 2) {
						if (args[0].equalsIgnoreCase("add")) {
							String word = getWord(args).toLowerCase();
							list.add(word);
							Chat.getInstance().getConfig()
									.set("Chat.Blacklist", list);
							Chat.getInstance().saveConfig();
							p.sendMessage(Messages.getAddedWordMSG(word));
							return true;
						} else if (args[0].equalsIgnoreCase("remove")) {
							String word = getWord(args).toLowerCase();
							if (list.contains(word)) {
								list.remove(word);
								Chat.getInstance().getConfig()
										.set("Chat.Blacklist", list);
								Chat.getInstance().saveConfig();
								p.sendMessage(Messages.getRemovedWordMSG(word));
								return true;
							} else {
								p.sendMessage(Messages
										.getWordNotInBlacklistMSG(word));
								return true;
							}
						}
					} else {
						p.sendMessage(Color
								.color("&8&m------------------------------------"));
						p.sendMessage(Color.color("&7> &e/cm add <word>"));
						p.sendMessage(Color.color("&7> &e/cm remove <word>"));
						p.sendMessage(Color.color("&7> &e/cm list"));
						p.sendMessage(Color
								.color("&8&m------------------------------------"));
					}
				}
			}
		}
		return false;
	}

	private static String getWord(String[] args) {
		StringBuilder sb = new StringBuilder();
		for (int i = 1; i < args.length; i++) {
			sb.append(args[i]);
		}
		return sb.toString();
	}

}
