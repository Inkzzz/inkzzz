package me.bukkit.aRepair.Inkzzz;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class RepairCMD implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String string,
			String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (cmd.getName().equalsIgnoreCase("fix")) {
				if (args.length == 0) {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',
							"&cUsage> &7/fix all"));
					return true;
				} else if (args.length == 1) {
					if (args[0].equalsIgnoreCase("all")) {
						if (p.hasPermission("repair.all")) {
							if (RepairHandle.inCooldown(p)) {
								if (RepairHandle.canRepair(p)) {
									for (ItemStack item : p.getInventory()
											.getContents()) {
										if (item != null) {
											if (RepairHandle.isRepairable(item))
												item.setDurability((short) 0);
										}
									}
									for (ItemStack item : p.getInventory()
											.getArmorContents()) {
										if (item != null)
											item.setDurability((short) 0);
									}
									p.sendMessage(ChatColor
											.translateAlternateColorCodes('&',
													"&cRepair> &7Your items have been repaired!"));
									RepairHandle.setCooldown(p);
									return true;
								} else {
									RepairHandle.sendCooldownMSG(p);
									return true;
								}
							} else {
								for (ItemStack item : p.getInventory()
										.getContents()) {
									if (item != null) {
										if (RepairHandle.isRepairable(item))
											item.setDurability((short) 0);
									}
								}
								for (ItemStack item : p.getInventory()
										.getArmorContents()) {
									if (item != null)
										item.setDurability((short) 0);
								}
								p.sendMessage(ChatColor
										.translateAlternateColorCodes('&',
												"&cRepair> &7Your items have been repaired!"));
								RepairHandle.setCooldown(p);
								return true;
							}
						} else
							p.sendMessage(ChatColor
									.translateAlternateColorCodes('&',
											"&cError> &7Sorry, but you don't have permission!"));
					}
				}
			}
		}
		return false;
	}
}
