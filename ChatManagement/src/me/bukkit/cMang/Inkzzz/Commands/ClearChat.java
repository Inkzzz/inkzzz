package me.bukkit.cMang.Inkzzz.Commands;

import me.bukkit.cMang.Inkzzz.Chat;
import me.bukkit.cMang.Inkzzz.Utils.Color;
import me.bukkit.cMang.Inkzzz.Utils.Messages;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ClearChat implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String string,
			String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(Color
					.color("&4Sorry, but you don't have permission!"));
			return true;
		}
		Player p = (Player) sender;

		if (cmd.getName().equalsIgnoreCase("cc")) {
			if (Chat.getInstance().hasPerm(p, "clearchat")) {

				for (int i = 0; i < 100; i++) {
					Bukkit.broadcastMessage(" ");
				}
				Bukkit.broadcastMessage(Messages.getClearChatMSG(p));
				return true;
			}
		}
		return false;
	}

}
